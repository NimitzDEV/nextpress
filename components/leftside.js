import React from 'react'
import styled from 'styled-components'
import { media } from '../tools/server'

const LeftBarContainer = styled.section`
  flex: 0 1 auto;
  background-color: #fff;
  color: #fff;
  width: 260px;
  height: 100vh;
  font-size: 16px;
  color: #000;
  display: flex;
  flex-direction: column;
  background-color: #fafafa;
  .content-section {
    flex: 0 1 auto;
    display: flex;
    flex-direction: column;
    .profpic {
      flex: 0 1 auto;
      width: 100%;
      heigh: auto;
    }
    .introduction {
      flex: 0 1 auto;
      .name-brand {
        font-size: 1.5em;
        text-align: center;
        margin: 16px;
        display: block;
      }
      .desc {
        font-size: 0.8em;
        text-align: center;
        letter-spacing: 2px;
        color: #cfd8dc;
        display: block;
      }
    }
  }
  .social-section {
    display: flex;
    flex: 0 1 auto;
    flex-wrap: wrap;
    align-items: space-around;
    justify-content: center;
    margin: 1em 0;
    .social-icon {
      text-decoration: none;
      font-size: 3em;
      text-align: center;
      flex: 1 1 auto;
      i {
        line-height: 3em;
      }
    }
  }
  ${media.tablet`
    height: 2em;
    width: 100vw;
    flex-direction: row;
    .content-section {
      flex-direction: row;
      flex: 1 1 auto;
      .profpic {
        display: inline-block;
      }
      .profpic {
        width: auto;
        height: 4em;
      }
      .introduction {
        display: flex;
        margin: 0.25em;
        flex-direction: column;
        justify-content: space-around;
        .name-brand {
          flex: 0 1 auto;
          margin: 0;
          text-transform: uppercase;
        }
        .desc {
          display: none;
        }
      }
    }
    .social-section {
      margin: 0;
      .social-icon {
        min-width: 0.5em;
        i {
          line-height: 1.5em;
        }
        span {
          display: none;
        }
      }
    }
  `};
`

const socialList = [
  {
    href: 'https://github.com/NimitzDEV',
    name: 'Github',
    icon: 'github',
    color: 'red',
    size: 'small'
  },
  {
    href: 'https://facebook.com/nimitzdev',
    name: 'Facebook',
    icon: 'facebook-official',
    color: 'orange',
    size: 'small'
  },
  {
    href: 'https://instagram.com/isnimitz',
    name: 'Instagram',
    icon: 'instagram',
    color: 'blue',
    size: 'small'
  },
  {
    href: 'https://twitter.com/nimitz5',
    name: 'Twitter',
    icon: 'twitter',
    color: 'violet',
    size: 'small'
  }
]

export default class AppLeftSide extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <LeftBarContainer>
        <div className="content-section">
          <img className="profpic" src="/static/profile.jpg" alt="NimitzDEV" />
          <div className="introduction">
            <span className="name-brand">NimitzDEV</span>
            <span className="desc">One life to live</span>
          </div>
        </div>
        <div className="social-section">
          {socialList.map(s => {
            return (
              <a
                className="social-icon"
                href={s.href}
                target="_blank"
                rel="noopener"
                key={Math.random()}
                aria-label={s.name}
                aria-hidden
              >
                <i
                  className={`fa fa-${s.icon}`}
                  color={s.color}
                  size={s.size}
                /> 
              </a>
            )
          })}
        </div>
      </LeftBarContainer>
    )
  }
}
