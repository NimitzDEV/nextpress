import React from 'react'
import styled from 'styled-components'

import { sizes, media } from '../../tools/server'

const ImgWrapper = styled.figure`
  width: 100%;
  position: relative;
`
const Image = styled.img`
  width: 100%;
  height: auto;
`
const Canvas = styled.canvas`
  width: 100%;
  height: auto;
`

const imageJPEGResolutionList = [50, 200, 630]
const imagePNGResolutionList = [50, 200, 630]

/**
 * Using CSS Property to apply blur effect on small images
 * User shoule pass down a img url which is original img 
 * that filename contains w and h characters
 * 
 * @export
 * @class DeferImg
 * @extends {React.Component}
 */
export default class DeferImg extends React.Component {
  constructor(props) {
    super(props)
    let match = props.src.match('(?:-original-w)(d+)(?:h)(d+)')
    let oh = match.pop()
    let ow = match.pop()
    this.state = {
      oWidth: ow,
      oHight: oh,
      cWidth: 0,
      cHeight: 0,
      idSuffix: Date.now() + '.' + Math.random()
    }
  }

  calculateHolderRect() {}

  // no webp support for now
  fallbackWebP() {}

  render() {
    return (
      <ImgWrapper>
        <Image
          id={`img-pre-holder-${this.state.idSuffix}`}
          src=""
          onLoad={() => {
            this.calculateHolderRect()
          }}
        />
        <Canvas id={`canvas-blur-${this.state.idSuffix}`} />
        {this.props.figcaption
          ? <figcaption>
              {this.props.figcaption}
            </figcaption>
          : null}
      </ImgWrapper>
    )
  }
}
