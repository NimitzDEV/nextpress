/**
 * This module is originally from
 * https://github.com/rexxars/react-markdown/
 * Thanks for the work!s
 */
const React = require('react')
const Parser = require('commonmark').Parser
const ReactRenderer = require('./commonmark')
const propTypes = require('prop-types')

const ReactMarkdown = function(props) {
  React.Component.call(this, props)
}

ReactMarkdown.prototype = Object.create(React.Component.prototype)
ReactMarkdown.prototype.constructor = ReactMarkdown

ReactMarkdown.prototype.render = function() {
  const containerProps = this.props.containerProps || {}
  const renderer = new ReactRenderer(this.props)
  const parser = new Parser(this.props.parserOptions)
  const ast = parser.parse(this.props.source || '')

  if (this.props.walker) {
    const walker = ast.walker()
    let event

    while ((event = walker.next())) {
      this.props.walker.call(this, event, walker)
    }
  }

  if (this.props.className) {
    containerProps.className = this.props.className
  }

  return React.createElement.apply(
    React,
    [
      this.props.containerTagName,
      containerProps,
      this.props.childBefore
    ].concat(renderer.render(ast).concat([this.props.childAfter]))
  )
}

ReactMarkdown.propTypes = {
  className: propTypes.string,
  containerProps: propTypes.object,
  source: propTypes.string.isRequired,
  containerTagName: propTypes.string,
  childBefore: propTypes.object,
  childAfter: propTypes.object,
  sourcePos: propTypes.bool,
  escapeHtml: propTypes.bool,
  skipHtml: propTypes.bool,
  softBreak: propTypes.string,
  allowNode: propTypes.func,
  allowedTypes: propTypes.array,
  disallowedTypes: propTypes.array,
  transformLinkUri: propTypes.func,
  transformImageUri: propTypes.func,
  unwrapDisallowed: propTypes.bool,
  renderers: propTypes.object,
  walker: propTypes.func,
  parserOptions: propTypes.object
}

ReactMarkdown.defaultProps = {
  containerTagName: 'div',
  parserOptions: {}
}

ReactMarkdown.types = ReactRenderer.types
ReactMarkdown.renderers = ReactRenderer.renderers
ReactMarkdown.uriTransformer = ReactRenderer.uriTransformer

module.exports = ReactMarkdown
