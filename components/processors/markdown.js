import React from 'react'
import ReactMarkdown from './mdrenderer/wrapper'

export default class Markdown extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <ReactMarkdown source={this.props.content || '# NO CONTENT AVAILABLE'} />
    )
  }
}
