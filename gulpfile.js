let gulp = require('gulp')
let pump = require('pump')
let responsive = require('gulp-responsive')

gulp.task('comp-image', function() {
  pump([
    gulp.src('static/img/*.{png,jpg}'),
    responsive(
      {
        '*.jpg': [
          {
            width: 50,
            rename: { suffix: '-w<width>h<height>@<scale>' }
          },
          {
            width: 200,
            rename: { suffix: '-w<width>h<height>@<scale>' }
          },
          {
            width: 630,
            rename: { suffix: '-w<width>h<height>@<scale>' }
          },
          {
            rename: { suffix: '-original-w<width>h<height>@<scale>' }
          }
        ],
        '*.png': [
          {
            width: 50,
            rename: { suffix: '-w<width>h<height>@<scale>' }
          },
          {
            width: 200,
            rename: { suffix: '-w<width>h<height>@<scale>' }
          },
          {
            width: 630,
            rename: { suffix: '-w<width>h<height>@<scale>' }
          },
          {
            rename: { suffix: '-w<width>h<height>@<scale>' }
          }
        ]
      },
      { quality: 70, withMetadata: false, progressive: true }
    ),
    gulp.dest('static/img/compressed/')
  ])
})
