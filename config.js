module.exports = {
  MONGO_ADDR: 'mongodb://127.0.0.1:27017/NextPress',
  JWT_SALT: 'PUT_SOME_RANDOM_STRING',
  API_PORT: 9001,
  VERSION: '1.0.0',
  HTTPS_CERT_FILE: '',
  HTTPS_CERT_KEY: '',
  DISQUS: {
    name: 'isnimitz'
  },
  jwt: {
    secret: 'D3fau1tT0kEn!',
    exp: 48
  }
}
